import pytest
from wordle_solver.solver import WordleSolver, LetterPositions

words = [
    'steward',
    'survival',
    'budge',
    'coerce',
    'ear',
    'corruption',
    'reinforce',
    'bar',
    'leader',
    'identification',
    'sister',
    'fill',
    'foundation',
    'folklore',
    'reliable',
    'golfs',
    'dogs',
    'alphabet'
]
words = [a.upper() for a in words]

class TestSolver:
    @pytest.mark.parametrize("test_input", words)
    def test_basic_solver(self, test_input):
        """
        Test basic Solving
        :return:
        """
        s = WordleSolver(test_input)
        s.solve()
        assert s.solved

    def test_basic_solver_single(self):
        """
        Test basic Solving
        :return:
        """
        s = WordleSolver("SINGLE")
        s.solve()
        assert s.solved

    def test_check_wrong_position(self):
        """
        Test the checker
        :return:
        """
        s = WordleSolver("DOGS")
        res = s.check("SGOD")
        for i in res:
            assert i['result'] == LetterPositions.WRONG_POSITION

    def test_check_right_position(self):
        """
        Test the checker
        :return:
        """
        s = WordleSolver("DOGS")
        res = s.check("DOGS")
        for i in res:
            assert i['result'] == LetterPositions.CORRECT

    def test_check_not_in_word(self):
        """
        Test the checker
        :return:
        """
        s = WordleSolver("DOGS")
        res = s.check("AAAA")
        for i in res:
            assert i['result'] == LetterPositions.NOT_IN_WORD

