from enum import Enum
from typing import List

from nltk.corpus import words

word_list = words.words()
import z3


class LetterPositions(Enum):
    NOT_IN_WORD = 1
    WRONG_POSITION = 2
    CORRECT = 3


class WordleSolver:
    def __init__(self, target_word, attempts=500):
        self.target_word = target_word
        self.solved = False
        self.attempts = attempts

        # self.letters = [z3.Int(f"letter_{x}") for x in range(0, len(self.target_word))]
        self.previous_attempts = []
        self.not_words = []
        self.words_constraints = None

    def solve(self) -> str:
        """
        Solve the wordle
        :return: Target String
        """
        guess = [z3.Int(f"guess_{x}") for x in range(0, len(self.target_word))]
        self.words_constraints = self.__get_words_constraints__(guess)
        while not self.solved and self.attempts > 0:
            self.step(guess)
            self.attempts -= 1

        return "TBD"

    def step(self, guess):
        """
        Perform one step evaluation of the wordle
        :return:
        """
        print(f"Attempts left: {self.attempts}")
        # Prepare a word to guess
        # Build our guess list
        word = self.__build_guess__(guess)

        # Guess the word and get results
        positions = self.check(word)

        # If all letters solved, then return
        if all(x['result'] == LetterPositions.CORRECT for x in positions):
            self.solved = True
            return

        self.previous_attempts.append(positions)

    def __get_words_constraints__(self, guesses):
        """
        Return constrains for the english language
        :return:
        """
        ands = []
        count = 0
        for word in word_list:
            if len(word) != len(self.target_word):
                continue
            word_constraints = []
            for i, letter in enumerate(word.upper()):
                word_constraints.append(guesses[i] == ord(letter))
            ands.append(z3.And(word_constraints))
        # solver.add(z3.Or(ands))
        return z3.Or(ands)

    def __build_guess__(self, guess) -> str:
        """
        Based on guessed sets, build an accurate guess
        :return: Word to guess
        """
        s = z3.Solver()


        # Add basic alphabetic constraints
        for letter in guess:
            s.add(letter <= ord('Z'))
            s.add(letter >= ord('A'))

        # Add the english dictionary
        # s.add(self.__get_words_constraints__())
        # self.__get_words_constraints__(s, guess)
        s.add(self.words_constraints)

        # Set Constraints for the known variables
        for attempt in self.previous_attempts:
            for i, item in enumerate(attempt):
                # If the item was correct, guess also is that letter in that location
                if item['result'] == LetterPositions.CORRECT:
                    s.add(guess[i] == ord(attempt[i]['letter']))

                # Not the current spot
                elif item['result'] == LetterPositions.WRONG_POSITION:
                    s.add(guess[i] != ord(attempt[i]['letter']))
                    ors = []
                    for j in [x for x in range(0, len(attempt)) if x != i]:
                        # But an or of the other spots
                        ors.append(guess[j] == ord(attempt[i]['letter']))
                    s.add(z3.Or(ors))

                # Letter is not in this word
                else:
                    # Letter not in word
                    for j in guess:
                        s.add(guess[i] != ord(attempt[i]['letter']))

        # If no previous attempts, Don't allow dupes
        if not self.previous_attempts:
            s.add(z3.Distinct(guess))

        s.check()
        m = s.model()
        word = ''.join([chr(m[i].as_long()) for i in guess])
        return word

    def check(self, word: str) -> List[LetterPositions]:
        """
        Check a word and return back what is found
        :param word: Word we are checking
        :return: List of LetterPositions
        """
        if len(word) != len(self.target_word):
            raise Exception("Attempting to check word of wrong size")

        res = []
        for i, letter in enumerate(word):
            if self.target_word[i] == letter:
                res.append({
                    'letter': letter,
                    'result': LetterPositions.CORRECT}
                )
            elif letter in self.target_word:
                res.append({
                    'letter': letter,
                    'result': LetterPositions.WRONG_POSITION}
                )
            else:
                res.append({
                    'letter': letter,
                    'result': LetterPositions.NOT_IN_WORD}
                )
        return res
